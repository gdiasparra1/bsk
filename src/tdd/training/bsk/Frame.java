package tdd.training.bsk;

public class Frame {
	private int fThrow;
	private int sThrow;
	private int bonus;
	static final int MAX_PINS_PER_FRAME=10;
	
	/**
	 * It initializes a frame given the pins knocked down in the first and second
	 * throws, respectively.
	 * 
	 * @param firstThrow  The pins knocked down in the first throw.
	 * @param secondThrow The pins knocked down in the second throw.
	 * @throws BowlingException
	 */
	public Frame(int firstThrow, int secondThrow) throws BowlingException {
		if(firstThrow>=0 && firstThrow<=MAX_PINS_PER_FRAME && secondThrow>=0 && secondThrow<=MAX_PINS_PER_FRAME) {
			if(firstThrow+secondThrow <= MAX_PINS_PER_FRAME) {		
				fThrow=firstThrow;
				sThrow = secondThrow;
			}
			else {
				throw new BowlingException();
			}
		}else {
			throw new BowlingException();
		}

	}

	/**
	 * It returns the pins knocked down in the first throw of this frame.
	 * 
	 * @return The pins knocked down in the first throw.
	 */
	public int getFirstThrow() {
		return fThrow;
	}

	/**
	 * It returns the pins knocked down in the second throw of this frame.
	 * 
	 * @return The pins knocked down in the second throw.
	 */
	public int getSecondThrow() {
		return sThrow;
	}

	/**
	 * It sets the bonus of this frame.
	 *
	 * @param bonus The bonus.
	 */
	public void setBonus(int bonus) {
		this.bonus=bonus;
	}

	/**
	 * It returns the bonus of this frame.
	 *
	 * @return The bonus.
	 */
	public int getBonus() {
		return bonus;
	}

	/**
	 * It returns the score of this frame (including the bonus).
	 * 
	 * @return The score
	 */
	public int getScore() { 
		return fThrow + sThrow +bonus;
	}

	/**
	 * It returns whether, or not, this frame is strike.
	 * 
	 * @return <true> if strike, <false> otherwise.
	 */
	public boolean isStrike() {
		return(fThrow== MAX_PINS_PER_FRAME);
		
	}

	/**
	 * It returns whether, or not, this frame is spare.
	 * 
	 * @return <true> if spare, <false> otherwise.
	 */
	public boolean isSpare() {
		return(this.getScore()== MAX_PINS_PER_FRAME);		
	}

}

package tdd.training.bsk;

import java.util.ArrayList;

public class Game {
	private ArrayList <Frame> listFrame= new ArrayList<Frame>();
	private int numberOfFramesAdded=0;
	private int valueOfFirstBonusThrow=0;
	private int valueOfSecondBonusThrow=0;
	public static final int NUMBER_OF_FRAMES=10;

	/**
	 * It initializes an empty bowling game.
	 */
	public Game() {
		// To be implemented
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) throws BowlingException {
		listFrame.add(frame);
		numberOfFramesAdded++;
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) throws BowlingException {
		return listFrame.get(index);
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) throws BowlingException {
		this.valueOfFirstBonusThrow=firstBonusThrow;
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) throws BowlingException {
		this.valueOfSecondBonusThrow=secondBonusThrow;
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		return valueOfFirstBonusThrow;
		
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		return valueOfSecondBonusThrow;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() throws BowlingException {
		int sumScore=0;
		int actualFrameScore=0;
		int strikeCounter=0;
		
		for(int i=0;i<NUMBER_OF_FRAMES;i++) {
			if(listFrame.get(i).isStrike() ) {
				strikeCounter++;
			}
		}
		
		if(strikeCounter!=10) {
		
			for(int i=0;i<NUMBER_OF_FRAMES;i++) {
				actualFrameScore=listFrame.get(i).getScore();
				
				/* you have to check if the throw is at maximum the sixth (starting from 0) because you cannot have 
				 * the possibility to take the second strike and the two subsequent throws
				 */
			if(i<7 && listFrame.get(i).isStrike() && listFrame.get(i+1).isStrike()) {	
				listFrame.get(i).setBonus(listFrame.get(i+1).getFirstThrow() + listFrame.get(i+2).getFirstThrow());
				listFrame.get(i+1).setBonus(listFrame.get(i+1).getFirstThrow() + listFrame.get(i+1).getSecondThrow());
				
				}else if(listFrame.get(i).isStrike() && i!=Frame.MAX_PINS_PER_FRAME-1) {
					listFrame.get(i).setBonus(listFrame.get(i+1).getFirstThrow() + listFrame.get(i+1).getSecondThrow());		
				
					}else if(listFrame.get(i).isSpare() && i!=Frame.MAX_PINS_PER_FRAME-1) {
						listFrame.get(i).setBonus(listFrame.get(i+1).getFirstThrow());}
			
			actualFrameScore=listFrame.get(i).getScore();
			sumScore=sumScore+actualFrameScore;
			}
			
			if(hasFirstBonusThrow()) {
				sumScore=sumScore+valueOfFirstBonusThrow;
			}
			
			if(hasSecondBonusThrow()) {
				sumScore=sumScore+valueOfSecondBonusThrow;
			}
		}else {
			sumScore=300;
		}
		
			
		return sumScore;
	}
	
	/**
	 * Returns the actual number of added frames at the games
	 * @return number of frames
	 */
	public int getNumberFramesInserted() {
		return numberOfFramesAdded;
	}

	public void calculateBonus(Frame actualFrame) {
		int indexActualFrame=listFrame.indexOf(actualFrame);
		int indexSubsequentFrame=indexActualFrame+1;
		
		Frame subsequentFrame=listFrame.get(indexSubsequentFrame);
		actualFrame.setBonus(subsequentFrame.getFirstThrow());
		
	}

	public boolean hasFirstBonusThrow() {
		return (listFrame.get(Frame.MAX_PINS_PER_FRAME-1).isSpare() );	
	}

	public boolean hasSecondBonusThrow() {
		return (listFrame.get(Frame.MAX_PINS_PER_FRAME-1).isStrike() );	
	}
	

}

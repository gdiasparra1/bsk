package bsk;

import static org.junit.Assert.*;

import org.junit.Test;

import tdd.training.bsk.BowlingException;
import tdd.training.bsk.Frame;
import tdd.training.bsk.Game;


public class FrameTest {

	@Test 
	public void testFrameFirstThrow() throws Exception{
		Frame frame=new Frame(2,4);
		assertEquals(2,frame.getFirstThrow());
	}
	
	@Test 
	public void testFrameSecondThrow() throws Exception{
		Frame frame=new Frame(2,4);
		assertEquals(4,frame.getSecondThrow());
	}
	
	@Test (expected = BowlingException.class)
	public void testCreatingNotValidFrame() throws Exception{
		Frame frame=new Frame(-1,-1);
	}
	
	@Test (expected = BowlingException.class)
	public void testCreatingNotValidFrameTooMuchPinsDown() throws Exception{
		Frame frame=new Frame(6,6);
	}
	
	@Test 
	public void testScore() throws Exception{
		Frame frame=new Frame(2,6);
		assertEquals(8,frame.getScore());
	}
	
	
	@Test 
	public void testPickTheCorrectFrameInTheGame() throws Exception{
		Game game = new Game();
		
		Frame frame0=new Frame(1,5);
		Frame frame1=new Frame(3,6);
		Frame frame2=new Frame(7,2);
		Frame frame3=new Frame(3,6);
		Frame frame4=new Frame(4,4);
		Frame frame5=new Frame(5,3);
		Frame frame6=new Frame(3,3);
		Frame frame7=new Frame(4,5);
		Frame frame8=new Frame(8,1);
		Frame frame9=new Frame(2,6);
		
		game.addFrame(frame0);
		game.addFrame(frame1);
		game.addFrame(frame2);
		game.addFrame(frame3);
		game.addFrame(frame4);
		game.addFrame(frame5);
		game.addFrame(frame6);
		game.addFrame(frame7);
		game.addFrame(frame8);
		game.addFrame(frame9);
		
		assertEquals(frame4,game.getFrameAt(4));
	}
		
	
	@Test 
	public void testAssignBonusBecauseOfSpare() throws Exception{
		Game game = new Game();
		
		Frame frame0=new Frame(1,9);
		Frame frame1=new Frame(3,6);
		Frame frame2=new Frame(7,2);
		Frame frame3=new Frame(3,6);
		Frame frame4=new Frame(4,4);
		Frame frame5=new Frame(5,3);
		Frame frame6=new Frame(3,3);
		Frame frame7=new Frame(4,5);
		Frame frame8=new Frame(8,1);
		Frame frame9=new Frame(2,6);
		
		game.addFrame(frame0);
		game.addFrame(frame1);
		game.addFrame(frame2);
		game.addFrame(frame3);
		game.addFrame(frame4);
		game.addFrame(frame5);
		game.addFrame(frame6);
		game.addFrame(frame7);
		game.addFrame(frame8);
		game.addFrame(frame9);
		
		frame0.setBonus(frame1.getFirstThrow());
				
		assertEquals(3,frame0.getBonus());
	}
	
	@Test 
	public void testFrameIsSpare() throws Exception{
		Game game = new Game();
		
		Frame frame0=new Frame(1,9);
		Frame frame1=new Frame(3,6);
		Frame frame2=new Frame(7,2);
		Frame frame3=new Frame(3,6);
		Frame frame4=new Frame(4,4);
		Frame frame5=new Frame(5,3);
		Frame frame6=new Frame(3,3);
		Frame frame7=new Frame(4,5);
		Frame frame8=new Frame(8,1);
		Frame frame9=new Frame(2,6);
		
		game.addFrame(frame0);
		game.addFrame(frame1);
		game.addFrame(frame2);
		game.addFrame(frame3);
		game.addFrame(frame4);
		game.addFrame(frame5);
		game.addFrame(frame6);
		game.addFrame(frame7);
		game.addFrame(frame8);
		game.addFrame(frame9);
			
				
		assertEquals(true,frame0.isSpare());
	}
	
	@Test 
	public void testFrameIsStrike() throws Exception{
		Game game = new Game();
		
		Frame frame0=new Frame(10,0);
		Frame frame1=new Frame(3,6);
		Frame frame2=new Frame(7,2);
		Frame frame3=new Frame(3,6);
		Frame frame4=new Frame(4,4);
		Frame frame5=new Frame(5,3);
		Frame frame6=new Frame(3,3);
		Frame frame7=new Frame(4,5);
		Frame frame8=new Frame(8,1);
		Frame frame9=new Frame(2,6);
		
		game.addFrame(frame0);
		game.addFrame(frame1);
		game.addFrame(frame2);
		game.addFrame(frame3);
		game.addFrame(frame4);
		game.addFrame(frame5);
		game.addFrame(frame6);
		game.addFrame(frame7);
		game.addFrame(frame8);
		game.addFrame(frame9);
			
				
		assertEquals(true,frame0.isStrike());
	}


}

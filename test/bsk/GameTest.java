package bsk;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import tdd.training.bsk.Frame;
import tdd.training.bsk.Game;

public class GameTest {
	
	@Test 
	public void testValidGame() throws Exception{
		Game game = new Game();
		
		Frame frame0=new Frame(1,5);
		Frame frame1=new Frame(3,6);
		Frame frame2=new Frame(7,2);
		Frame frame3=new Frame(3,6);
		Frame frame4=new Frame(4,4);
		Frame frame5=new Frame(5,3);
		Frame frame6=new Frame(3,3);
		Frame frame7=new Frame(4,5);
		Frame frame8=new Frame(8,1);
		Frame frame9=new Frame(2,6);
		
		game.addFrame(frame0);
		game.addFrame(frame1);
		game.addFrame(frame2);
		game.addFrame(frame3);
		game.addFrame(frame4);
		game.addFrame(frame5);
		game.addFrame(frame6);
		game.addFrame(frame7);
		game.addFrame(frame8);
		game.addFrame(frame9);
		
		
		assertEquals(10,game.getNumberFramesInserted());
	}
	
	@Test 
	public void testSumScoreOfGame() throws Exception{
		Game game = new Game();
		
		Frame frame0=new Frame(1,5);
		Frame frame1=new Frame(3,6);
		Frame frame2=new Frame(7,2);
		Frame frame3=new Frame(3,6);
		Frame frame4=new Frame(4,4);
		Frame frame5=new Frame(5,3);
		Frame frame6=new Frame(3,3);
		Frame frame7=new Frame(4,5);
		Frame frame8=new Frame(8,1);
		Frame frame9=new Frame(2,6);
		
		game.addFrame(frame0);
		game.addFrame(frame1);
		game.addFrame(frame2);
		game.addFrame(frame3);
		game.addFrame(frame4);
		game.addFrame(frame5);
		game.addFrame(frame6);
		game.addFrame(frame7);
		game.addFrame(frame8);
		game.addFrame(frame9);
		
		assertEquals(81,game.calculateScore());
	}
	
	@Test 
	public void testScoreOfGameWithASpare() throws Exception{
		Game game = new Game();
		
		Frame frame0=new Frame(1,9);
		Frame frame1=new Frame(3,6);
		Frame frame2=new Frame(7,2);
		Frame frame3=new Frame(3,6);
		Frame frame4=new Frame(4,4);
		Frame frame5=new Frame(5,3);
		Frame frame6=new Frame(3,3);
		Frame frame7=new Frame(4,5);
		Frame frame8=new Frame(8,1);
		Frame frame9=new Frame(2,6);
		
		game.addFrame(frame0);
		game.addFrame(frame1);
		game.addFrame(frame2);
		game.addFrame(frame3);
		game.addFrame(frame4);
		game.addFrame(frame5);
		game.addFrame(frame6);
		game.addFrame(frame7);
		game.addFrame(frame8);
		game.addFrame(frame9);
			
				
		assertEquals(88,game.calculateScore());
	}
		
	
	@Test 
	public void testScoreOfGameWithAStrike() throws Exception{
		Game game = new Game();
		
		Frame frame0=new Frame(10,0);
		Frame frame1=new Frame(3,6);
		Frame frame2=new Frame(7,2);
		Frame frame3=new Frame(3,6);
		Frame frame4=new Frame(4,4);
		Frame frame5=new Frame(5,3);
		Frame frame6=new Frame(3,3);
		Frame frame7=new Frame(4,5);
		Frame frame8=new Frame(8,1);
		Frame frame9=new Frame(2,6);
		
		game.addFrame(frame0);
		game.addFrame(frame1);
		game.addFrame(frame2);
		game.addFrame(frame3);
		game.addFrame(frame4);
		game.addFrame(frame5);
		game.addFrame(frame6);
		game.addFrame(frame7);
		game.addFrame(frame8);
		game.addFrame(frame9);
			
				
		assertEquals(94,game.calculateScore());
	}
		
	
	@Test 
	public void testScoreOfGameWithStrikeFollowedBySpare() throws Exception{
		Game game = new Game();
		
		Frame frame0=new Frame(10,0);
		Frame frame1=new Frame(4,6);
		Frame frame2=new Frame(7,2);
		Frame frame3=new Frame(3,6);
		Frame frame4=new Frame(4,4);
		Frame frame5=new Frame(5,3);
		Frame frame6=new Frame(3,3);
		Frame frame7=new Frame(4,5);
		Frame frame8=new Frame(8,1);
		Frame frame9=new Frame(2,6);
		
		game.addFrame(frame0);
		game.addFrame(frame1);
		game.addFrame(frame2);
		game.addFrame(frame3);
		game.addFrame(frame4);
		game.addFrame(frame5);
		game.addFrame(frame6);
		game.addFrame(frame7);
		game.addFrame(frame8);
		game.addFrame(frame9);
			
				
		assertEquals(103,game.calculateScore());
	}
	
	
	@Test 
	public void testScoreOfGameWithMultipleStrike() throws Exception{
		Game game = new Game();
		
		Frame frame0=new Frame(10,0);
		Frame frame1=new Frame(10,0);
		Frame frame2=new Frame(7,2);
		Frame frame3=new Frame(3,6);
		Frame frame4=new Frame(4,4);
		Frame frame5=new Frame(5,3);
		Frame frame6=new Frame(3,3);
		Frame frame7=new Frame(4,5);
		Frame frame8=new Frame(8,1);
		Frame frame9=new Frame(2,6);
		
		game.addFrame(frame0);
		game.addFrame(frame1);
		game.addFrame(frame2);
		game.addFrame(frame3);
		game.addFrame(frame4);
		game.addFrame(frame5);
		game.addFrame(frame6);
		game.addFrame(frame7);
		game.addFrame(frame8);
		game.addFrame(frame9);
			
				
		assertEquals(112,game.calculateScore());
	}
	
	
	@Test 
	public void testScoreOfGameWithMultipleSpares() throws Exception{
		Game game = new Game();
		
		Frame frame0=new Frame(8,2);
		Frame frame1=new Frame(5,5);
		Frame frame2=new Frame(7,2);
		Frame frame3=new Frame(3,6);
		Frame frame4=new Frame(4,4);
		Frame frame5=new Frame(5,3);
		Frame frame6=new Frame(3,3);
		Frame frame7=new Frame(4,5);
		Frame frame8=new Frame(8,1);
		Frame frame9=new Frame(2,6);
		
		game.addFrame(frame0);
		game.addFrame(frame1);
		game.addFrame(frame2);
		game.addFrame(frame3);
		game.addFrame(frame4);
		game.addFrame(frame5);
		game.addFrame(frame6);
		game.addFrame(frame7);
		game.addFrame(frame8);
		game.addFrame(frame9);
			
				
		assertEquals(98,game.calculateScore());
	}
	
	@Test 
	public void testSpareAtLastFrameGiveFirstBonusThrow() throws Exception{
		Game game = new Game();
		
		Frame frame0=new Frame(1,5);
		Frame frame1=new Frame(3,6);
		Frame frame2=new Frame(7,2);
		Frame frame3=new Frame(3,6);
		Frame frame4=new Frame(4,4);
		Frame frame5=new Frame(5,3);
		Frame frame6=new Frame(3,3);
		Frame frame7=new Frame(4,5);
		Frame frame8=new Frame(8,1);
		Frame frame9=new Frame(2,8);
		
		game.addFrame(frame0);
		game.addFrame(frame1);
		game.addFrame(frame2);
		game.addFrame(frame3);
		game.addFrame(frame4);
		game.addFrame(frame5);
		game.addFrame(frame6);
		game.addFrame(frame7);
		game.addFrame(frame8);
		game.addFrame(frame9);
			
				
		assertEquals(true,game.hasFirstBonusThrow());
	}
	
	@Test 
	public void testSpareAtLastFrame() throws Exception{
		Game game = new Game();
		
		Frame frame0=new Frame(1,5);
		Frame frame1=new Frame(3,6);
		Frame frame2=new Frame(7,2);
		Frame frame3=new Frame(3,6);
		Frame frame4=new Frame(4,4);
		Frame frame5=new Frame(5,3);
		Frame frame6=new Frame(3,3);
		Frame frame7=new Frame(4,5);
		Frame frame8=new Frame(8,1);
		Frame frame9=new Frame(2,8);
		
		game.addFrame(frame0);
		game.addFrame(frame1);
		game.addFrame(frame2);
		game.addFrame(frame3);
		game.addFrame(frame4);
		game.addFrame(frame5);
		game.addFrame(frame6);
		game.addFrame(frame7);
		game.addFrame(frame8);
		game.addFrame(frame9);
		
		game.setFirstBonusThrow(7);
				
		assertEquals(90,game.calculateScore());
	}
	
	
	@Test 
	public void testStrikeAtLastFrameGiveSecondBonusThrow() throws Exception{
		Game game = new Game();
		
		Frame frame0=new Frame(1,5);
		Frame frame1=new Frame(3,6);
		Frame frame2=new Frame(7,2);
		Frame frame3=new Frame(3,6);
		Frame frame4=new Frame(4,4);
		Frame frame5=new Frame(5,3);
		Frame frame6=new Frame(3,3);
		Frame frame7=new Frame(4,5);
		Frame frame8=new Frame(8,1);
		Frame frame9=new Frame(10,0);
		
		game.addFrame(frame0);
		game.addFrame(frame1);
		game.addFrame(frame2);
		game.addFrame(frame3);
		game.addFrame(frame4);
		game.addFrame(frame5);
		game.addFrame(frame6);
		game.addFrame(frame7);
		game.addFrame(frame8);
		game.addFrame(frame9);
			
				
		assertEquals(true,game.hasSecondBonusThrow());
	}
	
	@Test 
	public void testStrikeAtLastFrame() throws Exception{
		Game game = new Game();
		
		Frame frame0=new Frame(1,5);
		Frame frame1=new Frame(3,6);
		Frame frame2=new Frame(7,2);
		Frame frame3=new Frame(3,6);
		Frame frame4=new Frame(4,4);
		Frame frame5=new Frame(5,3);
		Frame frame6=new Frame(3,3);
		Frame frame7=new Frame(4,5);
		Frame frame8=new Frame(8,1);
		Frame frame9=new Frame(10,0);
		
		game.addFrame(frame0);
		game.addFrame(frame1);
		game.addFrame(frame2);
		game.addFrame(frame3);
		game.addFrame(frame4);
		game.addFrame(frame5);
		game.addFrame(frame6);
		game.addFrame(frame7);
		game.addFrame(frame8);
		game.addFrame(frame9);
		
		game.setFirstBonusThrow(7);
		game.setSecondBonusThrow(2);
				
		assertEquals(92,game.calculateScore());
	}
	
	@Test 
	public void testPerfectGame() throws Exception{
		Game game = new Game();
		
		Frame frame0=new Frame(10,0);
		Frame frame1=new Frame(10,0);
		Frame frame2=new Frame(10,0);
		Frame frame3=new Frame(10,0);
		Frame frame4=new Frame(10,0);
		Frame frame5=new Frame(10,0);
		Frame frame6=new Frame(10,0);
		Frame frame7=new Frame(10,0);
		Frame frame8=new Frame(10,0);
		Frame frame9=new Frame(10,0);
		
		game.addFrame(frame0);
		game.addFrame(frame1);
		game.addFrame(frame2);
		game.addFrame(frame3);
		game.addFrame(frame4);
		game.addFrame(frame5);
		game.addFrame(frame6);
		game.addFrame(frame7);
		game.addFrame(frame8);
		game.addFrame(frame9);
		
		game.setFirstBonusThrow(10);
		game.setSecondBonusThrow(10);
				
		assertEquals(300,game.calculateScore());
	}
	
}
